import socket

MENU = """GET#albumList() - print list of albums
GET#albumSongs(albumName) - print a list of all the album’s songs
GET#songLen(songName) - print the length of the song
GET#songWords(songName) - print all the song’s words
GET#whatAlbum(songName) - print the album of the song
GET#nameSearch(word) - print all the songs that have this word in their name
GET#wordSearch(word) - print all the songs that have this word in their lyrics	
EXIT#exit() - exits the programme
"""

def server():
    LISTEN_PORT = 4242
    with socket.socket() as listening_sock:
        listening_sock.bind(('', LISTEN_PORT))
        listening_sock.listen(1)
        client_soc, client_address = listening_sock.accept()
        with client_soc:
            msg = "welcome to our pink fluid knowledge protocol, hope you have the worst time using it"
            client_soc.sendall(msg.encode())
            print(msg)

            client_msg = client_soc.recv(1024)
            client_msg = client_msg.decode()
            print(client_msg)

            msg = "why do you keep doing this, go touch some grass!!, anyway here the commands"
            client_soc.sendall(msg.encode())
            print(msg)

            while(client_msg != "EXIT#exit()"):
                client_soc.sendall(MENU.encode())
                print(MENU)

                client_msg = client_soc.recv(1024)
                client_msg = client_msg.decode()
                print(client_msg)
                msg_copy = client_msg

                if("(" in client_msg and ")" in client_msg): # not necessary but I thing it looks better
                    start = msg_copy.split("(")
                    end = msg_copy.split(")")
                    msg_copy = "()".join([start[0], end[1]])

                    if (len(start) > 2 or len(end) > 2):
                        msg_copy = "error"

                print(msg_copy)

                if(client_msg == "GET#albumList()"):
                    msg = "RESPONSE#albumList#none# <list>\n"
                    client_soc.sendall(msg.encode())
                    print(msg)

                elif(msg_copy == "GET#albumSongs()"):
                    msg = "RESPONSE#albumSongs#album#<list>\n"
                    client_soc.sendall(msg.encode())
                    print(msg)

                elif(msg_copy == "GET#songLen()"):
                    msg = "RESPONSE#songLen#song#length\n"
                    client_soc.sendall(msg.encode())
                    print(msg)

                elif(msg_copy == "GET#songWords()"):
                    msg = "RESPONSE#songWords#song#”...”\n"
                    client_soc.sendall(msg.encode())
                    print(msg)

                elif(msg_copy == "GET#whatAlbum()"):
                    msg = "RESPONSE#whatAlbum#song#album\n"
                    client_soc.sendall(msg.encode())
                    print(msg)

                elif(msg_copy == "GET#nameSearch()"):
                    msg = "RESPONSE#nameSearch#name#”...”\n"
                    client_soc.sendall(msg.encode())
                    print(msg)

                elif(msg_copy == "GET#wordSearch()"):
                    msg = "RESPONSE#wordSearch#word#”...”\n"
                    client_soc.sendall(msg.encode())
                    print(msg)

                elif(client_msg == "EXIT#exit()"):
                    print("okey that's it (:")

                else:
                    msg = "syntaxError\n"
                    client_soc.sendall(msg.encode())
                    print(msg)

            msg = "tenk yu end ev e greyt dey (:"
            client_soc.sendall(msg.encode())
            print(msg)

def main():
    server()

if __name__ == "__main__":
    main()
