import socket

def client(): # the client
    SERVER_IP = "127.0.0.1"
    SERVER_PORT = 4242

    with socket.socket(type=socket.SOCK_DGRAM) as sock:

        # Create a TCP socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        server_address = (SERVER_IP, SERVER_PORT)
        sock.connect(server_address)

        server_msg = sock.recv(1024)
        server_msg = server_msg.decode()
        print(server_msg)

        msg = input()
        sock.sendall(msg.encode()) # sending the msg

        server_msg = sock.recv(1024)
        server_msg = server_msg.decode()
        print(server_msg)



        while(msg != "EXIT#exit()"):
            server_msg = sock.recv(1024)
            server_msg = server_msg.decode()
            print(server_msg)

            msg = input()
            sock.sendall(msg.encode()) # sending the msg

            server_msg = sock.recv(1024)
            server_msg = server_msg.decode()
            print(server_msg)

def main():
    client()

if __name__ == "__main__":
    main()
