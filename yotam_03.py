import socket

# the func gets the information and senfd it to the movie server

#this func bans every thing that isn't a musical (:
# if you want to ban actors just change the "Musical" to an actor or dsomthing else
def ban_every_non_musical(msg):
    if ("Musical" not in msg):
        msg = 'ERROR#"you fool, you moron"' # let's just let him know the true...
    return msg

# this func bans every year after 2022, we can also use the the specific time and this day commend but I think it's no necessary for here
def ban_year_after_2022(msg, returned_msg):
    time = ((msg.split("-")[1]).split("&"))[0] # this will give us the year
    print("time = " + time + " returned_msg = " + returned_msg)
    if(int(time) > 2022 and "MOVIEDATA#" in returned_msg):
        returned_msg = 'ERROR#"year cannot be after 2022"'
    return returned_msg

def client_server(cliant_msg):
    # makeing it also a cliant part
    SERVER_IP = "54.71.128.194"
    SERVER_PORT = 92

    server_msg = "" # we need to make the msg stay outside of the with bound

    # Create a non-specific UDP socket
    with socket.socket(type=socket.SOCK_DGRAM) as sock:

        # Create a TCP socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # Connecting to remote computer 92
        server_address = (SERVER_IP, SERVER_PORT)
        sock.connect(server_address)

        sock.sendall(cliant_msg.encode()) # sending the msg
        server_msg = sock.recv(1024)
        server_msg = server_msg.decode()

        if("MOVIEDATA#" in server_msg):
            server_msg = server_msg.replace("jpg", ".jpg") # makeing the photo valid
            msg_list = server_msg.split('"')
            msg_list[1] = msg_list[1] + " proxi" # adding the proxi to the name
            server_msg = '"'.join(msg_list)
    return server_msg

def listen_server():
    LISTEN_PORT = 9090

    with socket.socket() as listening_sock:
        listening_sock.bind(('', LISTEN_PORT))
        listening_sock.listen(1)
        client_soc, client_address = listening_sock.accept()
        with client_soc:
            client_msg = client_soc.recv(1024)

            client_msg = client_msg.decode()
            print(client_msg)

            client_list = client_msg.split(":")
            city = (client_list[3]).lower()
            if city != "france": # checking if the movie is from france
                returned_msg = client_server(client_msg)
                #returned_msg = ban_every_non_musical(returned_msg)
                # this is a line I made to ban every non musical, if you want to try it just remove the # in the past line
                returned_msg = ban_year_after_2022(client_msg, returned_msg)
                client_soc.sendall(returned_msg.encode())
                print(returned_msg)
            else:
                client_soc.sendall('ERROR#"France is banned!"'.encode()) # if it's from france we ban the movie
                print("France is banned!")


def main():
    while 0 == 0:
        listen_server()

if __name__ == "__main__":
    main()
